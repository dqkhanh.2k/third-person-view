import { Entity } from "playcanvas";
import { ColliderEvent } from "../collision/collider";
import CollisionManager from "../collision/collision_manager";
import Bullet, { BulletEvent } from "./bullet";

export default class BulletManager extends Entity {
    constructor(app){
        super("bullet manager");
        this.listBullets = [];
        this.app = app;
        this.shooting = false;
        this.shootSpeed = 100;
        this.lastBulletTime = 0;
    }

    update(dt) {
        if(this.shooting){
            if(Date.now() - this.lastBulletTime > this.shootSpeed){
                this._createNewBullet();
                this.lastBulletTime = Date.now();
                this.parent.fireParticle.particlesystem.reset()
                this.parent.fireParticle.particlesystem.play()
            }
        }
        this.listBullets.forEach(bullet => {
            bullet.update(dt)
        })
        
    }

    shoot(){
        this.lastBulletTime = Date.now();
        this.shooting = true;
    }

    _createNewBullet(){
        let bullet = new Bullet();
        
        bullet.setPosition(this.getPosition())
        this.parent.parent.addChild(bullet);
        bullet.setLocalScale(0.05, 0.05, 0.05)
        CollisionManager.getInstance().add(bullet.collider)

        this.parent.fireParticle.enabled = true;
        bullet.setRotation(this.parent.getRotation())
        bullet.rotateLocal(0,180,0);
        bullet.on(BulletEvent.Destroy, this.removeBullet, this);
        this.listBullets.push(bullet)
    }

    removeBullet(bullet){
        let index = this.listBullets.indexOf(bullet);
        if(index!==-1){
            this.listBullets.splice(index, 1);
            this.parent.parent.removeChild(bullet);
            CollisionManager.getInstance().remove(bullet.collider)
        }
    }
}