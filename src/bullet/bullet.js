import * as pc from "playcanvas";
import Collider, { ColliderEvent } from "../collision/collider";
import CollisionManager from "../collision/collision_manager";

export const BulletEvent = Object.freeze({
    Collided: "bulletevent:collided",
    Destroy: "bulletevent:destroy"
})

export default class Bullet extends pc.Entity {
    constructor() {
        super("Bullet");
        this.speed = 30;
        this.addComponent("model", {
            type: "sphere",
        });
        this.lifeTime = 2000;
        this.time = Date.now();
        this.isMoving = true;

        this.collider = new Collider(this.model.meshInstances[0].aabb);
        this.collider.on(ColliderEvent.Colliding, this._onCollision, this);
    }

    update(dt){
        if (this.isMoving) {
            this.translateLocal(0, 0, -this.speed*dt);
            if (Date.now() - this.time > this.lifeTime) {
                this.destroy();
            }
        }
    }

    copy() {
        let bullet = new Bullet(this.app);
        this.app.root.addChild(bullet);
        bullet.setPosition(this.getPosition());
        bullet.setLocalScale(0.1, 0.1, 0.1);
        return bullet;
    }

    _onCollision() {
        this.destroy()
    }

    destroy() {
        this.fire(BulletEvent.Destroy, this)
        this.isMoving = false;
        super.destroy();
    }
}
