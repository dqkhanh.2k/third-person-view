import * as pc from "playcanvas";

export class World extends pc.Entity {
    constructor() {
        super("world");
        this._init();
        
    }

    _init() {
        const light = new pc.Entity("light");
        light.addComponent("light", {
            color: [0.9, 0.8, 0.7],
            castShadows: true,
            intensity: 0.9,
            shadowBias: 0.2,

            shadowResolution: 2048,
            shadowDistance: 20,
            type: "directional",
        });
        this.addChild(light);
        light.setEulerAngles(20, 90, 0);

        this.plane = new pc.Entity("Plane");
        this.plane.addComponent("model", {
            type: "plane",
        });
        this.plane.setLocalScale(50, 50, 50);
        this.addChild(this.plane);

        this.box = new pc.Entity("target10");
        this.box.addComponent("model", {
            type: "box",
        });
        this.box.setPosition(10, 0.5, 0);

    }
}



