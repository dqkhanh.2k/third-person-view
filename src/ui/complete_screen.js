import { Entity, SCALEMODE_BLEND, Vec2, Vec4 } from "playcanvas";

export const CompleteScreenEvent = Object.freeze({
    Replay: "completescreenevent:replay",
    Next: "completescreenevent:next",
    Close: "completescreenevent:close",
});

export default class CompleteScreen extends Entity {
    constructor(app) {
        super("complete screen");
        this.app = app;
        this.addComponent("screen", {
            resolution: [1280, 720],
            referenceResolution: [1080, 1920],
            screenSpace: true,
            scaleMode: SCALEMODE_BLEND,
            scaleBlend: 0.5,
        });

        this._init();
    }

    _init() {
        this.imgComplete = new Entity("img complete");
        this.imgComplete.addComponent("element", {
            type: "image",
            textureAsset: this.app.assets.find("complete.png"),
            width: 700,
            height: 700,
            anchor: [0.5, 0.5, 0.5, 0.5],
            pivot: [0.5, 0.5],
        });

        this.btnNext = this._createImageButton("next", {
            type: "image",
            textureAsset: this.app.assets.find(`btn_next_level.png`),
            width: 200,
            height: 200,
            anchor: [1, 0, 1, 0],
            pivot: [1, 0],
            useInput: true,
        });
        this.btnNext.button.on("click", this.onNext, this);
        this.imgComplete.addChild(this.btnNext);

        this.btnReplay = this._createImageButton("replay", {
            type: "image",
            textureAsset: this.app.assets.find(`btn_replay.png`),
            width: 200,
            height: 200,
            anchor: [0, 0, 0, 0],
            pivot: [0, 0],
            useInput: true,
        });
        this.btnReplay.button.on("click", this.onReplay, this);
        this.imgComplete.addChild(this.btnReplay);

        this.btnClose = this._createImageButton("close", {
            type: "image",
            textureAsset: this.app.assets.find(`btn_close_ui.png`),
            width: 100,
            height: 100,
            anchor: [0.9, 0.9, 0.9, 0.9],
            pivot: [0.5, 0.5],
            useInput: true,
        });
        this.btnClose.button.on("click", this.onClose, this);
        this.imgComplete.addChild(this.btnClose);

        this.img1Star = new Entity("1start");
        this.img1Star.addComponent("element", {
            type: "image",
            textureAsset: this.app.assets.find("1_star.png"),
            width: 189,
            height: 182,
            anchor: [0.39, 0.39, 0.39, 0.39],
            pivot: [1, 0.5],
        });
        this.img1Star.setLocalScale(0.7, 0.7, 0.7);
        this.img1Star.enabled = false;
        this.imgComplete.addChild(this.img1Star);

        this.img2Star = new Entity("2start");
        this.img2Star.addComponent("element", {
            type: "image",
            textureAsset: this.app.assets.find("2_star.png"),
            width: 394,
            height: 228,
            anchor: [0.6, 0.35, 0.6, 0.35],
            pivot: [1, 0.5],
        });
        this.img2Star.setLocalScale(0.7, 0.7, 0.7);
        this.imgComplete.addChild(this.img2Star);
        this.img2Star.enabled = false;

        this.img3Star = new Entity("3start");
        this.img3Star.addComponent("element", {
            type: "image",
            textureAsset: this.app.assets.find("3_star.png"),
            width: 591,
            height: 247,
            anchor: [0.5, 0.33, 0.5, 0.33],
            pivot: [0.5, 0.5],
        });
        this.img3Star.setLocalScale(0.7, 0.7, 0.7);
        this.img3Star.enabled = false;
        this.imgComplete.addChild(this.img3Star);

        this.txtScoreResult = new Entity("txt score result");
        this.txtScoreResult.addComponent("element", {
            type: "text",
            anchor: [0.64, 0.65, 0.64, 0.65],
            pivot: [0.5, 0.5],
            text: "100",
            fontAsset: this.app.assets.find("font"),
            fontSize: 50,
            color: [0.588, 0.317, 0.13],
            autoWidth: true,
            autoHeight: true,
        });
        this.imgComplete.addChild(this.txtScoreResult);

        this.txtTimeResult = new Entity("txt health result");
        this.txtTimeResult.addComponent("element", {
            type: "text",
            anchor: [0.64, 0.55, 0.64, 0.55],
            pivot: [0.5, 0.5],
            text: "100",
            fontAsset: this.app.assets.find("font"),
            fontSize: 50,
            color: [0.588, 0.317, 0.13],
            autoWidth: true,
            autoHeight: true,
        });
        this.imgComplete.addChild(this.txtTimeResult);

        this.addChild(this.imgComplete);
    }

    _createImageButton(name, config) {
        let btn = new Entity(`btn ${name}`);
        btn.addComponent("element", config);
        btn.addComponent("button", {
            imageEntity: btn,
        });

        return btn;
    }

    gameOver(time, score) {
        this.imgComplete.element.textureAsset =
            this.app.assets.find(`game_over.png`);
        this.btnNext.enabled = false;
        this.btnReplay.element.pivot = new Vec2(0.5, 0.5);
        this.btnReplay.element.anchor = new Vec4(0.5, 0, 0.5, 0);
        this.txtScoreResult.element.anchor = new Vec4(0.64, 0.482, 0.64, 0.482);
        this.txtTimeResult.element.anchor = new Vec4(0.64, 0.4, 0.64, 0.4);
        this.txtScoreResult.element.text = score;
        this.txtTimeResult.element.text = time;
    }

    updateResult(time, score, percent) {
        this.txtScoreResult.element.text = score;
        this.txtTimeResult.element.text = time;
        if (percent > 0.6) {
            this.img3Star.enabled = true;
        } else if (percent > 0.3) {
            this.img2Star.enabled = true;
        } else {
            this.img1Star.enabled = true;
        }
    }

    hide() {
        this.img1Star.enabled = false;
        this.img2Star.enabled = false;
        this.img3Star.enabled = false;
        this.enabled = false;
    }

    onClose() {
        if (this.enabled) this.fire(CompleteScreenEvent.Close);
    }

    onReplay() {
        if (this.enabled) this.fire(CompleteScreenEvent.Replay);
    }

    onNext() {
        if (this.enabled) this.fire(CompleteScreenEvent.Next);
    }
}
