import { Entity } from "playcanvas";

export default class PlayInfoScreen extends Entity {
    constructor(app) {
        super("score screen");
        this.app = app;
        this.addComponent("screen", {
            resolution: [1280, 720],
            referenceResolution: [1080, 1920],
            screenSpace: true,
        });
        this._init();
    }

    _init() {
        this.imgHealth = new Entity("img health");
        this.imgHealth.addComponent("element", {
            type: "image",
            textureAsset: this.app.assets.find("health_count.png"),
            width: 200,
            height: 70,
            anchor: [0, 1, 0, 1],
            pivot: [0, 1],
        });
        this.addChild(this.imgHealth);

        this.txtHealth = new Entity("txt health");
        this.txtHealth.addComponent("element", {
            type: "text",
            anchor: [0.5, 0.5, 0.5, 0.5],
            pivot: [0, 0.5],
            text: "100",
            fontAsset: this.app.assets.find("font"),
            fontSize: 30,
            color: [0.588, 0.317, 0.13],
            autoWidth: true,
            autoHeight: true,
        });

        this.imgHealth.addChild(this.txtHealth);

        this.imgScore = new Entity("img score");
        this.imgScore.addComponent("element", {
            type: "image",
            textureAsset: this.app.assets.find("score_count.png"),
            width: 200,
            height: 70,
            anchor: [1, 1, 1, 1],
            pivot: [1, 1],
        });
        this.txtScore = new Entity("txt health");
        this.txtScore.addComponent("element", {
            type: "text",
            anchor: [0.5, 0.5, 0.5, 0.5],
            pivot: [0, 0.5],
            text: "0",
            fontAsset: this.app.assets.find("font"),
            fontSize: 30,
            color: [0.588, 0.317, 0.13],
            autoWidth: true,
            autoHeight: true,
        });

        this.imgScore.addChild(this.txtScore);
        this.addChild(this.imgScore)
    }

    updateHealth(health){
        this.txtHealth.element.text = health
    }

    updateScore(score){
        this.txtScore.element.text = score
    }

    
}
