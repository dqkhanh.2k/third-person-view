import * as pc from "playcanvas";

export default class ScrollScreen extends pc.Entity {
    constructor(app) {
        super("scroll screen");
        this.app = app;
        this.addComponent("screen", {
            resolution: [1280, 720],
            referenceResolution: [1080, 1920],
            screenSpace: true,
            scaleMode: pc.SCALEMODE_BLEND,
            scaleBlend: 0.5,
        });
        this.enabled = false;
        this._init();
    }

    _init() {
        const background = new pc.Entity("Background");
        background.addComponent("element", {
            type: "image",
            textureAsset: this.app.assets.find("large_screen.png"),
            width: 1300,
            height: 700,
            anchor: [0.5, 0.5, 0.5, 0.5],
            pivot: [0.5, 0.5],
        });
        this.addChild(background);

        let txtName = new pc.Entity("txt screen name");
        txtName.addComponent("element", {
            type: "text",
            anchor: [0.5, 0.14, 0.5, 0.14],
            pivot: [0.5, 0.5],
            text: "Choose level",
            fontAsset: this.app.assets.find("font"),
            fontSize: 70,
            color: [0.588, 0.317, 0.13],
            autoWidth: true,
            autoHeight: true,
        });
        background.addChild(txtName);

        this.btnClose = this._createImageButton("close", {
            type: "image",
            textureAsset: this.app.assets.find(`btn_close_ui.png`),
            width: 100,
            height: 100,
            anchor: [1, 1, 1, 1],
            pivot: [0.5, 0.5],
            useInput: true,
        });
        this.btnClose.button.on("click", this.onClose, this);
        background.addChild(this.btnClose);

        const content = this.createContent();

        const viewport = new pc.Entity("Viewport");
        viewport.addChild(content);

        viewport.addComponent("element", {
            anchor: new pc.Vec4(0, 0, 1, 1),
            color: new pc.Color(1, 0, 1),
            margin: new pc.Vec4(0, 20, 20, 0),
            mask: true,
            opacity: 1,
            pivot: new pc.Vec2(0, 1),
            rect: new pc.Vec4(0, 0, 1, 1),
            type: pc.ELEMENTTYPE_IMAGE,
            useInput: false,
        });

        const horizontalScrollbar = createScollbar(true);
        const verticalScrollbar = createScollbar(false);

        const scrollview = new pc.Entity("ScrollView");
        scrollview.addChild(viewport);
        scrollview.addChild(verticalScrollbar);

        this.addChild(scrollview);

        scrollview.addComponent("element", {
            anchor: new pc.Vec4(0.5, 0.54, 0.5, 0.54),
            height: 500,
            pivot: new pc.Vec2(0.5, 0.5),
            type: pc.ELEMENTTYPE_GROUP,
            useInput: false,
            width: 1120,
        });

        scrollview.addComponent("scrollview", {
            bounceAmount: 0.1,
            contentEntity: content,
            friction: 0.05,
            horizontal: false,
            horizontalScrollbarEntity: horizontalScrollbar,
            horizontalScrollbarVisibility:
                pc.SCROLLBAR_VISIBILITY_SHOW_WHEN_REQUIRED,
            scrollMode: pc.SCROLL_MODE_BOUNCE,
            vertical: true,
            verticalScrollbarEntity: verticalScrollbar,
            verticalScrollbarVisibility:
                pc.SCROLLBAR_VISIBILITY_SHOW_WHEN_REQUIRED,
            viewportEntity: viewport,
        });
    }

    onClose(){
        this.enabled = false
    }

    createContent() {
        const content = new pc.Entity("content");

        let num = 55;
        let height = Math.floor(num / 10) * 135;
        content.addComponent("element", {
            anchor: new pc.Vec4(0, 1, 0, 1),
            height: height,
            pivot: new pc.Vec2(0, 1),
            type: pc.ELEMENTTYPE_GROUP,
            useInput: true,
            width: 1150,
        });

        content.addComponent("layoutgroup", {
            orientation: pc.ORIENTATION_HORIZONTAL,
            reverseX: false,
            reverseY: true,
            alignment: new pc.Vec2(0, 1),
            padding: new pc.Vec4(0, 20, 0, 20),
            spacing: new pc.Vec2(10, 10),
            wrap: true,
        });

        for (let i = 0; i < num; i++) {
            let btn = this._createImageButton("btn", {
                type: "image",
                textureAsset: this.app.assets.find(`level.png`),
                width: 100,
                height: 100,
                anchor: new pc.Vec4(0, 1, 0, 1),
                margin: new pc.Vec4(0, 0, 0, 0),
                pivot: [0.5, 0.5],
                useInput: true,
            });
            let txt = new pc.Entity("txt level");
            txt.addComponent("element", {
                type: "text",
                anchor: [0.5, 0.58, 0.5, 0.58],
                pivot: [0.5, 0.5],
                text: i,
                fontAsset: this.app.assets.find("font"),
                fontSize: 32,
                color: [0.588, 0.317, 0.13],
                autoWidth: true,
                autoHeight: true,
            });
            btn.addChild(txt);
            content.addChild(btn);
        }

        return content;
    }

    _createImageButton(name, config) {
        let btn = new pc.Entity(`btn ${name}`);
        btn.addComponent("element", config);
        btn.addComponent("button", {
            imageEntity: btn,
        });

        return btn;
    }
}

function createScollbar(horizontal) {
    const handle = new pc.Entity("Handle");
    const handleOptions = {
        type: pc.ELEMENTTYPE_IMAGE,
        color: new pc.Color(1, 1, 1),
        opacity: 1,
        margin: new pc.Vec4(0, 0, 0, 0),
        rect: new pc.Vec4(0, 0, 1, 1),
        mask: false,
        useInput: true,
    };
    if (horizontal) {
        handleOptions.anchor = new pc.Vec4(0, 0, 0, 1); // Split in Y

        handleOptions.pivot = new pc.Vec2(0, 0); // Bottom left
    } else {
        handleOptions.anchor = new pc.Vec4(0, 1, 1, 1); // Split in X

        handleOptions.pivot = new pc.Vec2(1, 1); // Top right
    }
    handle.addComponent("element", handleOptions);
    handle.addComponent("button", {
        active: true,
        imageEntity: handle,
        hitPadding: new pc.Vec4(0, 0, 0, 0),
        transitionMode: pc.BUTTON_TRANSITION_MODE_TINT,
        hoverTint: new pc.Color(1, 1, 1),
        pressedTint: new pc.Color(1, 1, 1),
        inactiveTint: new pc.Color(1, 1, 1),
        fadeDuration: 0,
    });

    const scrollbar = new pc.Entity(
        horizontal ? "HorizontalScrollbar" : "VerticalScrollbar"
    );

    scrollbar.addChild(handle);

    const scrollbarOptions = {
        type: pc.ELEMENTTYPE_IMAGE,
        color: new pc.Color(0.5, 0.5, 0.5),
        opacity: 1,
        rect: new pc.Vec4(0, 0, 1, 1),
        mask: false,
        useInput: false,
    };

    const scrollbarSize = 10;

    if (horizontal) {
        scrollbarOptions.anchor = new pc.Vec4(0, 0, 1, 0);

        scrollbarOptions.pivot = new pc.Vec2(0, 0);

        scrollbarOptions.margin = new pc.Vec4(
            0,
            0,
            scrollbarSize,
            -scrollbarSize
        );
    } else {
        scrollbarOptions.anchor = new pc.Vec4(1, 0, 1, 1);

        scrollbarOptions.pivot = new pc.Vec2(1, 1);

        scrollbarOptions.margin = new pc.Vec4(
            -scrollbarSize,
            scrollbarSize,
            0,
            0
        );
    }
    scrollbar.addComponent("element", scrollbarOptions);
    scrollbar.addComponent("scrollbar", {
        orientation: horizontal
            ? pc.ORIENTATION_HORIZONTAL
            : pc.ORIENTATION_VERTICAL,
        value: 0,
        handleSize: 0.5,
        handleEntity: handle,
    });

    return scrollbar;
}
