import { EventHandler } from "playcanvas";

export const LevelEvent = Object.freeze({
    Start: "level:start",
    Complete: "level:complete",
    Failure: "level:failure",
    Scored: "level:scored"
});

export default class Level extends EventHandler {
    constructor(data) {
        super();
        this.targetSpeed = data.target_speed;
        this.numTarget = data.number_of_target;
        this.maxTime = data.max_time;
        this.started = false;
    }

    start(){
        this.startTime = Date.now()
        this.started = true
    }

    update(player){
        if(!this.started)
            return
        if(player.health <= 0 || Date.now() - this.startTime() > this.maxTime){
            this._levelFail()
        }
        else if(player.killTarget === this.numTarget){
            this._levelComplete()
        }
    }

    _levelComplete() {
        this.fire(LevelEvent.Complete, this);
    }

    _levelFail() {
        this.fire(LevelEvent.Failure, this);
    }
}
