import {EventHandler} from 'playcanvas'
import { LevelEvent } from "./level";

export const LevelManagerEvent = Object.freeze({
  Start:    "levelmanager:start",
  Finish:   "levelmanager:finish",
  LoadLevel: "levelmanager:loadlevel",
});

export default class LevelManager extends EventHandler {
  constructor() {
    super();

    /** @type {Array<Level>} */
    this.levels = [];
    this.curLevelIndex = 0;
    
  }

  addLevel(level) {
    this.levels.push(level);
  }

  start() {
    if (this.levels.length <= 0) {
      return;
    }

    this.startLevel(0);
  }

  startLevel(index) {
    this.curLevelIndex = index;
    if (this.curLevelIndex >= this.levels.length) {
      return;
    }
    let level = this.levels[this.curLevelIndex];
    level.start()
    this.fire(LevelManagerEvent.LoadLevel, level)
  }

  nextLevel() {
    this.startLevel(this.curLevelIndex + 1);
  }

  replayLevel(){
    this.startLevel(this.curLevelIndex)
  }

}