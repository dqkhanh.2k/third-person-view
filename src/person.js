import * as pc from "playcanvas";
import AssetManager from "./asset_manager";
import Bullet from "./bullet/bullet";
import BulletManager from "./bullet/bullet_manager";
import Camera from "./camera";
import Collider, { ColliderEvent } from "./collision/collider.js";
import CollisionManager from "./collision/collision_manager";

export const PersonEvent = Object.freeze({
    Collided: "personevent:collided",
    Dead: "personevent:dead",
})

export default class Person extends pc.Entity {
    constructor(app) {
        super("person");
        this.setPosition(10, 0, 20)
        this.app = app;
        this.health = 100
        this._init();
        this._initBoundingBox()
        this._initFirstView();

    }

    _init() {
        const light = new pc.Entity("player_light");
        light.addComponent("light", {
            color: [1, 1, 1],
            castShadows: false,
            intensity: 1,
        });
        light.setLocalEulerAngles(90, 0, 0);
        this.addChild(light);

        this.addComponent("model", {
            type: "asset",
            asset: this.app.assets.find("person"),
        });

        AssetManager.loadMaterial(this.model.model, "person", this.app);

        this.cameraAxis = new Camera();
        this.addChild(this.cameraAxis);

        this._initWeapon();

        this.direction = "Idle";
        this.blendTime = 0.2;
    }

    _initBoundingBox(){
        
        let pos = this.getPosition();
        let halfExtends = new pc.Vec3(0.3836, 0.9, 0.3836)
        this.boundingBox = new pc.BoundingBox(pos, halfExtends)

        this.collider = new Collider(this.boundingBox);
        this.collider.on(ColliderEvent.Colliding, this.onCollided, this);
        CollisionManager.getInstance().add(this.collider, "player")
        
    }

    onCollided(){
        this.health -= 10;
        this.fire(PersonEvent.Collided, this.health);
        if(this.health<=0){
            this.fire(PersonEvent.Dead)
        }
    }

    addAnimation(animations = []) {
        this.addComponent("animation", {
            asset: animations,
            speed: 1,
            loop: true,
            playback: true,
        });
        this.animation.assets = animations;
    }

    setDirection = function (direction) {
        this.animation.play(direction, this.blendTime);
    };

    _initFirstView() {
        this.firstCamera = new pc.Entity("first_camera");
        this.firstCamera.addComponent("camera", {
            clearColor: new pc.Color(0.11, 0.1, 0.1),
            clearDepthBuffer: true,
        });
        this.firstCamera.camera.enabled = false
        this.firstCamera.setLocalPosition(-0.15, 1.35, 0.25)
        this.addChild(this.firstCamera);
    }

    _initWeapon() {
        this.weapon = new pc.Entity("weapon");
        this.weapon.addComponent("model", {
            type: "asset",
            asset: this.app.assets.find("ak47"),
        });
        this.findByName("swat:RightHandIndex1").addChild(this.weapon);
        this.weapon.setLocalPosition(-17, 0, -5);
        this.weapon.setLocalScale(2.5, 2.5, 2.5);
        this.weapon.setLocalEulerAngles(-90, 170, -10);

        this._initBullet()
        this._initFireParticle();

        AssetManager.loadMaterial(this.weapon.model.model, "ak47", this.app);
    }

    _initFireParticle() {
        this.fireParticle = new pc.Entity("fire_particle");
        this.fireParticle.addComponent("particlesystem", {
            enabled: true,
            autoPlay: false,
            loop: false,
            numParticles: 1,
            lifetime: 0.1,
            rate: 0.2,
            rate2: 0.02,
            intensity: 2,
            blendType: 1,
            stretch: 5,
            emitterShape: 1,
            localVelocityGraph: {
                type: 1,
                keys: [
                    [0, -5],
                    [0, 0],
                    [0, 0],
                ],
                betweenCurves: false,
            },
            scaleGraph: {
                type: 1,
                keys: [0, 0.0374, 0.9907, 0.0437],
                betweenCurves: false,
            },
            colorGraph: {
                type: 4,
                keys: [
                    [0, 0.9922, 0.6031, 0.9922, 0.9969, 0.9922],
                    [0, 0.6745, 0.6031, 0.4118, 0.9969, 0.3412],
                    [0, 0.1176, 0.6031, 0.11765, 0.9969, 0.11765],
                ],
                betweenCurves: false,
            },
            alphaGraph: {
                type: 1,
                keys: [0, 0.99375, 1, 0],
                betweenCurves: false,
            },
        });
        this.weapon.addChild(this.fireParticle);
        this.fireParticle.setLocalPosition(15, 2.8, -0.27);
    }

    update(dt){
        let pos = this.getLocalPosition()
        this.boundingBox.center.x = pos.x;
        this.boundingBox.center.z = pos.z;
        
        this.bulletManager.update(dt)
    }

    _initBullet(){
        this.bulletManager = new BulletManager();
        this.bulletManager.setLocalScale(0.02, 0.02, 0.02)
        this.addChild(this.bulletManager);
        this.bulletManager.setLocalPosition(-0.22,1.22, 0.8)
    }

    shoot(){
        this.bulletManager.shoot()
    }

    endShoot(){
        this.bulletManager.shooting = false;
    }
}
