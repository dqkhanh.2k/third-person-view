import * as pc from "playcanvas";

export default class MovementEvent {
    constructor(app, person, cameraMovement) {
        this.app = app;
        this.person = person;
        this.speed = 0.05;
        this.cameraMovement = cameraMovement;
        this.keyboard = this.app.keyboard;
        this.mouse = this.app.mouse;
        this.direction = "Idle";

        this.keyboard.on(pc.EVENT_KEYDOWN, this.keyChange, this);

        this.keyboard.on(pc.EVENT_KEYUP, this.keyChange, this);
        this.mouse.on(pc.EVENT_MOUSEDOWN, this.mouseDown, this);
        this.mouse.on(pc.EVENT_MOUSEUP, this.mouseUp, this);
        this.mouse.on(pc.EVENT_MOUSEDOWN, this.keyChange, this);
        this.mouse.on(pc.EVENT_MOUSEUP, this.keyChange, this);
    }

    update(delta) {
        let forward = this.person.forward;
        let right = this.person.right;

        let x = 0;
        let z = 0;

        if (this.keyboard.isPressed(pc.KEY_D)) {
            x -= right.x;
            z -= right.z;
        }

        if (this.keyboard.isPressed(pc.KEY_A)) {
            x += right.x;
            z += right.z;
        }

        if (this.keyboard.isPressed(pc.KEY_S)) {
            x += forward.x;
            z += forward.z;
        }

        if (this.keyboard.isPressed(pc.KEY_W)) {
            x -= forward.x;
            z -= forward.z;
        }

        if (x !== 0 || z !== 0) {
            let pos = new pc.Vec3(x * delta, 0, z * delta);
            pos.normalize().scale(this.speed);
            pos.add(this.person.getPosition());

            var targetY = this.cameraMovement.eulers.x;
            var rot = new pc.Vec3(0, targetY, 0);
            this.person.setPosition(pos.x, pos.y, pos.z);
            this.person.setEulerAngles(rot);
        }
    }

    keyChange() {
        let tempDirection = this.direction;

        this.checkActions();

        if (tempDirection !== this.direction) {
            this.person.setDirection(this.direction);
        }
    }

    mouseDown(e){
        if (this.mouse.isPressed(pc.MOUSEBUTTON_LEFT)) {
            if(this.app.root.findByName("complete screen").enabled) return;
                this.person.shoot()
        }
        if (this.mouse.isPressed(pc.MOUSEBUTTON_RIGHT)) {
            this.switchCamera()
        }
    }

    mouseUp(e){
        if(e.button === pc.MOUSEBUTTON_LEFT && this.app.root.findByName("complete screen").enabled) return;
                this.person.endShoot()
    }


    checkActions() {
        let w = this.keyboard.isPressed(pc.KEY_W);
        let a = this.keyboard.isPressed(pc.KEY_A);
        let s = this.keyboard.isPressed(pc.KEY_S);
        let d = this.keyboard.isPressed(pc.KEY_D);

        if (this.mouse.isPressed(pc.MOUSEBUTTON_LEFT) && !this.person.firstCamera.camera.enabled) {
            this.direction = "Firing Rifle";
        } else if (w && !s) {
            if (a && !d) {
                this.direction = "Run Forward Left";
            } else if (d && !a) {
                this.direction = "Run Forward Right";
            } else {
                this.direction = "Run Forward";
            }
        } else if (s && !w) {
            if (a && !d) {
                this.direction = "Run Backward Left";
            } else if (d && !a) {
                this.direction = "Run Backward Right";
            } else {
                this.direction = "Run Backward";
            }
        } else if (a && !d) {
            this.direction = "Run Left";
        } else if (d && !a) {
            this.direction = "Run Right";
        } else {
            this.direction = "Idle";
        }
    }

    shooting(e) {
        
    }

    switchCamera(){
        this.person.cameraAxis.cameraEntity.camera.enabled = false;
        if(this.person.firstCamera.camera.enabled){
            this.person.cameraAxis.cameraEntity.camera.enabled = true;
            this.person.firstCamera.camera.enabled = false;
            
            this.person.weapon.setLocalEulerAngles(-90, 170, -10);
        }
        else{
            this.person.firstCamera.camera.enabled = true;
            this.person.firstCamera.setLocalEulerAngles(
                this.person.getEulerAngles()
            );
            this.person.weapon.setLocalEulerAngles(-90, 170, -20);
        }
            
    }
}
