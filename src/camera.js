import * as pc from "playcanvas";

export default class Camera extends pc.Entity {
    constructor() {
        super("Camera axis");
        this.setPosition(0.5, 0.7, 0);
        this.cameraEntity = new pc.Entity("third_camera");
        this.cameraEntity.setPosition(-3, 0.4, 3);
        this.cameraEntity.setEulerAngles(0, -46, 0);
        this.cameraEntity.addComponent("camera", {
            clearColor: new pc.Color(0.11, 0.1, 0.1),
        });
        this.cameraEntity.camera.enabled = true
        console.log(this.cameraEntity.camera)

        this.rayEnd = new pc.Entity("RaycastrayEnd");
        this.rayEnd.setPosition(0, 0, 4.43)

        this.addChild(this.cameraEntity);
        this.addChild(this.rayEnd)
    }
}
