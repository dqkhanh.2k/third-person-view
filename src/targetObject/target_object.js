import { Entity, Vec2 } from "playcanvas";
import Collider, { ColliderEvent } from "../collision/collider";
import CollisionManager from "../collision/collision_manager";

export default class TargetObject extends Entity {
    constructor() {
        super("target");
        this.addComponent("model", {
            type: "capsule",
        });
        this.enabled = false;

        this.collider = new Collider(this.model.meshInstances[0].aabb);
        
        this.collider.on(ColliderEvent.Colliding, () => {
            this.enabled = false;
        });
    }

    update(dt, person, speed) {
        let pos = person.getPosition();
        let p = this.getPosition();
        let dx = pos.x - p.x;
        let dz = pos.z - p.z;
        let normalVec = new Vec2(dx, dz).normalize();
        this.setPosition(
            p.x + normalVec.x * speed * dt,
            1,
            p.z + normalVec.y * speed * dt
        );
    }
}
