import { Entity } from "playcanvas";
import { ColliderEvent } from "../collision/collider.js";
import CollisionManager from "../collision/collision_manager.js";
import { LevelEvent } from "../level/level.js";
import Target from "./target_object.js";

export default class TargetManager extends Entity {
    constructor() {
        super();
        this.listTarget = [];
        this.targetNum = 0;
        this.currentActive = 0;
    }

    start(number) {
        this.targetNum = number;
        this.hitTarget = 0;
        let targetNeed = number - this.listTarget.length
        if(targetNeed > 0){
            this.randomNewTarget(targetNeed)
        }
        this.randomTarget()
        let i = 0;
        let intervalId = setInterval(() => {
            if(i === number)
                clearInterval(intervalId)
            if(this.listTarget[i]){
                this.listTarget[i].enabled = true;
                CollisionManager.getInstance().add(this.listTarget[i].collider, "target")
            }
                
                i++
        }, 2000)
    }

    randomTarget() {
        this.listTarget.forEach((target) => {
            if (!target.enabled) {
                let x = getRandomInt(25);
                let z = getRandomInt(25);
                target.translate(x, 1, z);
            }
        });
    }

    randomNewTarget(sum) {
        for (let i = 0; i < sum; i++) {
            let capsule = new Target();
            this.listTarget.push(capsule);
            capsule.collider.on(ColliderEvent.Colliding, () => this.updateHitTarget(capsule))
            this.addChild(capsule);
        }
    }

    removeTarget(target) {
        let index = this.listTarget.indexOf(target);
        if (index !== -1) {
            this.listTarget.splice(index, 1);
        }
    }

    updateHitTarget(target){
        this.hitTarget++;
        CollisionManager.getInstance().remove(target.collider);
        if(this.hitTarget >= this.targetNum){
            this.fire(LevelEvent.Complete)
        }
    }
}

function getRandomInt(max) {
    let rd = Math.floor(Math.random() * max * 2);
    let a = Math.floor(Math.random() * 2);
    if (a == 0) return -Math.floor(rd / 2);
    else return Math.floor(rd / 2);
}
