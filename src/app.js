import * as pc from "playcanvas";
import Person, { PersonEvent } from "./person.js";
import PlayerMovement from "./player_event.js";
import { World } from "./world.js";
import AssetManager from "./asset_manager.js";
import CameraMovement from "./camera_movement.js";
import PlayInfoScreen from "./ui/play_info.js";
import LevelManager, { LevelManagerEvent } from "./level/level_manager.js";
import LevelLoader, { LevelLoaderEvent } from "./level/level_loader.js";
import TargetManager from "./targetObject/target_manager.js";
import CollisionManager, { CollisionManagerEvent } from "./collision/collision_manager.js";
import CompleteScreen, { CompleteScreenEvent } from "./ui/complete_screen.js";
import { LevelEvent } from "./level/level.js";
import ScrollScreen from "./ui/scroll_screen.js";

export default class App {
    constructor(canvas, useKeyboard = true, useMouse = true) {
        this.config = {
            keyboard: useKeyboard ? new pc.Keyboard(window) : null,
            mouse: useMouse ? new pc.Mouse(canvas) : null,
            elementInput: new pc.ElementInput(canvas),
        };
        this.score = 0;
        this.levelScore = 0;
        this.pause = false
        this.app = new pc.Application(canvas, this.config);
        this.assetManager = new AssetManager(this.app);
        this.assetManager.loadAsset();
    }

    _init() {
        this.app.setCanvasFillMode(pc.FILLMODE_FILL_WINDOW);
        this.app.setCanvasResolution(pc.RESOLUTION_AUTO);
        window.addEventListener("resize", () => this.app.resizeCanvas());


        this.scrollScreen = new ScrollScreen(this.app);
        this.app.root.addChild(this.scrollScreen)

        this.world = new World();
        this.app.root.addChild(this.world);

        this.person = new Person(this.app);
        let listAnimations = this.app.assets.list().filter((e) => {
            if (e.type === "animation") return e;
        });
        this.person.addAnimation(listAnimations);
        this.app.root.addChild(this.person);

        this.playInfo = new PlayInfoScreen(this.app);
        this.app.root.addChild(this.playInfo);
        this.person.on(PersonEvent.Collided, this.playInfo.updateHealth, this.playInfo)
        this.person.on(PersonEvent.Dead, this.gameOver, this)

        this.levelManager = new LevelManager();
        this.levelLoader = new LevelLoader();
        this.targetManager = new TargetManager();

        this.collisionManager = CollisionManager.getInstance()
        this.app.root.addChild(this.targetManager);
        this.collisionManager.on(CollisionManagerEvent.HitTarget, this.onHitTarget, this)
        
        this.completeScreen = new CompleteScreen(this.app);
        this.app.root.addChild(this.completeScreen);
        this.completeScreen.enabled = false;
        this.completeScreen.on(CompleteScreenEvent.Next, this.nextLevel, this)
        this.completeScreen.on(CompleteScreenEvent.Close, this.onClose, this)
        this.completeScreen.on(CompleteScreenEvent.Replay, this.reload, this)

        this.targetManager.on(LevelEvent.Complete, this.completeLevel, this)

        this.levelLoader.on(
            LevelLoaderEvent.Load,
            this.levelManager.addLevel,
            this.levelManager
        );
        this.levelLoader.once(
            LevelLoaderEvent.Load,
            this.levelManager.start,
            this.levelManager
        );

        this.levelManager.on(
            LevelManagerEvent.LoadLevel,
            this.startLevel,
            this
        );

        this.levelLoader.load();

        this.app.on("update", this.update, this)
    }

    onClose(){
        this.completeScreen.enabled = false;
        this.scrollScreen.enabled = true;
    }

    reload(){
        window.location.reload()
    }

    nextLevel(){
        this.completeScreen.hide();
        this.levelManager.nextLevel();
        this.pause = false;
    }

    gameOver(){
        this.completeScreen.enabled = true;
        let score = this.score;
        let time = parseInt((Date.now() - this.level.startTime) / 1000);
        this.completeScreen.gameOver(time, score);
        this.pause = true;
        this.app.mouse.disablePointerLock()
    }

    completeLevel(){
        
        this.completeScreen.enabled = true;
        let score = this.score;
        let time = parseInt((Date.now() - this.level.startTime) / 1000);
        let percent = this.levelScore/10 / this.level.numTarget;

        if(this.person.health <= 10)
            return
        
        this.completeScreen.updateResult(time, score, percent)
        this.pause = true;
        this.app.mouse.disablePointerLock()
    }

    onHitTarget(){
        this.score+= 10;
        this.levelScore += 10;
        this.playInfo.updateScore(this.score)
    }

    update(dt){
        if(!this.pause){
            this.targetManager.listTarget.forEach(target => {
            target.update(dt, this.person, this.level.targetSpeed)
            })
            this.collisionManager.update();
            this.person.update(dt)
        }
        
    }

    startLevel(level) {
        this.levelScore = 0;
        this.level = level;
        this.targetManager.start(this.level.numTarget)
    }

    _initEvent() {
        this.cameraMovement = new CameraMovement(this.app, this.person);
        this.playerMovement = new PlayerMovement(
            this.app,
            this.person,
            this.cameraMovement
        );

        this.app.on("update", (delta) => {
            this.cameraMovement.update(delta);
            this.playerMovement.update(delta);
        });
    }

    start() {
        this.app.preload(() => {
            this._init();
            this._initEvent();
            this.app.start();
        });
    }
}
