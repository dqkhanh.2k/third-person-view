import * as pc from "playcanvas";

export default class CameraMovement {
    constructor(app, person) {
        this.app = app;
        this.person = person;
        this.eulers = new pc.Vec3();
        this.mouseSpeed = 2;

        this.app.mouse.on("mousedown", () =>{
            if(!this.app.root.findByName("complete screen").enabled &&
            !this.app.root.findByName("scroll screen").enabled)
            this.app.mouse.enablePointerLock()
        }

        );
        this.app.mouse.on("mousemove", this.mouseMove.bind(this));
    }

    update(delta) {
        let targetY = this.eulers.x + 180;
        let targetX = this.eulers.y;
        let targetAng = new pc.Vec3(-targetX, targetY, 0);

        this.person.cameraAxis.setEulerAngles(targetAng);
        this.person.cameraAxis.cameraEntity.setPosition(this._getWorldPoint());
        this.person.cameraAxis.cameraEntity.lookAt(
            this.person.cameraAxis.getPosition()
        );

        if(this.app.mouse.isPressed(pc.MOUSEBUTTON_LEFT)){
            this.person.setEulerAngles(0, this.eulers.x, 0);
        }

        if (this.person.firstCamera.camera.enabled) {
            this.person.setEulerAngles(0, this.eulers.x, 0);
            this.person.firstCamera.setEulerAngles(-targetX, targetY, 0);
            let spine = this.person.findByName("swat:Spine");
            let angle = spine.getEulerAngles();

            spine.setEulerAngles(targetX, angle.y, 0)
        }
    }

    _getWorldPoint() {
        return this.person.cameraAxis.rayEnd.getPosition();
    }

    mouseMove(e) {
        if (pc.Mouse.isPointerLocked()) {
            this.ex = e.dx;
            this.ey = e.dy;
            this.eulers.x -= ((this.mouseSpeed * e.dx) / 60) % 360;
            this.eulers.y += ((this.mouseSpeed * e.dy) / 60) % 360;

            if (this.eulers.x < 0) this.eulers.x += 360;
            if (this.eulers.y < 0) this.eulers.y += 360;
        }
    }
}
