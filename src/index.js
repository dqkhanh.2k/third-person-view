import App from "./app";
const container = document.querySelector(".container");
const progress = document.getElementById("progress-value");
const canvas = document.getElementById("canvas");

window.onload = () => {
    const app = new App(canvas);
    app.start();
    

    app.app.on("preload:progress", setProgress);
    app.app.on("preload:end", function () {
        app.app.off("preload:progress");
        container.style.display = "none";
        canvas.style.display = 'inline'
    });
};

function setProgress(value) {
    progress.style.width = value * 100 + "%";
}
