import {EventHandler} from 'playcanvas';

export const ColliderEvent = Object.freeze({
    Colliding: "collider:colliding",
});

export default class Collider extends EventHandler {
    constructor(aabb){
        super()
        this.boundingBox = aabb;
    }

    checkCollision(collider){
        let result = this.boundingBox.intersects(collider.boundingBox);
        if(result){
            this.onCollision()
            collider.onCollision()
        }
        return result
    }

    onCollision(){
        this.fire(ColliderEvent.Colliding, this)
    }

}