import { EventHandler } from "playcanvas";

export const CollisionManagerEvent = Object.freeze({
    HitTarget: "collisionmanager:hittarget",
    HitPlayer: "collisionmanager:hitplayer"
})

export const ColliderType = Object.freeze({
    Bullet: 'bullet',
    Target: 'target',
    Player: 'player'
})

export default class CollisionManager extends EventHandler {
    constructor() {
        super()
        this.listColliders = {
            bullet: [],
            target: [],
            player: []
        };
    }

    static getInstance(){
        if(!this.instance)
            this.instance = new CollisionManager();
        return this.instance
        
    }

    add(collider, type = ColliderType.Bullet) {
        if (!this.listColliders[type])
            this.listColliders[type] = [];
        this.listColliders[type].push(collider);
    }

    remove(collider) {
        
        for (const key in this.listColliders) {
            if (this.listColliders[key].includes(collider)) {
                let index = this.listColliders[key].indexOf(collider);
                this.listColliders[key].splice(index, 1);
                return;
            }
        }
    }

    update() {
        this.listColliders.target.forEach((targetCollider) => {

            this.listColliders.player.forEach(playerCollider => {
                let result = targetCollider.checkCollision(playerCollider)
                if(result){
                    this.remove(targetCollider)
                    this.fire(CollisionManagerEvent.HitPlayer, targetCollider, playerCollider)
                }
            })

            this.listColliders.bullet.forEach(bulletCollider => {
                if(targetCollider.checkCollision(bulletCollider)){
                    this.fire(CollisionManagerEvent.HitTarget, targetCollider, bulletCollider)
                    this.remove(bulletCollider)
                }
            })

            
        })
    }
}
